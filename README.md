# albina-base-map

Source GIS-project for the base-map of the avalanche report homepage. 

Contains properties for five zoom layers. The first three have been revised and updated with e.g. a high resolution DEM of austria and italy.
The new two added zoom layers comprise more peaks, cities as well as symbols of gondolas and chairlifts in the last layer. Each layer can be adjusted under the specific property settings in QGIS in version 3 or newer.

**Sources and Licences**

Digital elevation models:

- Tyrol: DEM 10m
https://www.data.gv.at/katalog/dataset/8aec8dbb-a7d5-4deb-b0e6-54c1d6028e76
- Italy: DEM 20m (compiled and resampled von Sonny
 ;E-Mail:  sonnyy7@gmail.com
 Website: http://data.opendataportal.at/dataset/dtm-italy)
    - Regione Abruzzo: DTM 10 m
http://opendata.regione.abruzzo.it/content/modello-digitale-del-terreno-risoluzione-10x10-metri

    - Regione Basilicata: DTM 5 m
http://rsdi.regione.basilicata.it/dbgt-ctr/

    - Regione Calabria: DTM 5 m
http://geoportale.regione.calabria.it/opendata

    - Citta Metropolitana di Napoli: DTM 1 m
http://sit.cittametropolitana.na.it/lidar.html

    - Regione Autonoma Friuli Venezia Giulia: DTM 10 m
http://irdat.regione.fvg.it/CTRN/ricerca-cartografia/

    - Regione Lazio: DTM 5 m
http://dati.lazio.it/weblist/cartografia/prodotti/2002_2003_DTM_5mt_DXF/

    - Regione Liguria: Modello Digitale del Terreno da CTR 1:5000 dal 2007 II ed. 3D/DB (= DTM 5 m)
http://www.regione.liguria.it/sep-servizi-online/catalogo-servizi-online/opendata/item/7192-dtm-modello-digitale-del-terreno-da-ctr-15000-dal-2007-ii-ed-3ddb-topografico-passo-5-m.html

    - Regione Lombardia: DTM5x5 Modello Digitale del Terreno 2015 (= DTM 5 m)
http://www.geoportale.regione.lombardia.it/en/download-ricerca

    - Regione Piemonte: RIPRESA AEREA ICE 2009-2011 - DTM (= DTM 5 m)
http://www.geoportale.piemonte.it/geocatalogorp/index.jsp

    - Regione Puglia: DTM 8 m
http://www.sit.puglia.it/portal/portale_cartografie_tecniche_tematiche/Download/Cartografie

    - Regione Autonoma della Sardegna, Sardegna Geoportale: DTM 1 m and DTM 10 m
http://www.sardegnageoportale.it/webgis2/sardegnamappe/?map=download_raster
DTM 1m, DTM MATTM 1m - Aree critiche, DTM MATTM 1m -  Costa Gallura, DTM MATTM 1m - Fasce fluviali, DTM 10m (digitized DTM based on a Topomap with 10 m contour lines)

    - Regione Siciliana: MDT 2012-2013 2x2 ETRS89  (= DTM 2 m)
http://www.sitr.regione.sicilia.it/geoportalen/sfoglia-il-catalogo/

    - Regione Toscana: DTM 10 m
http://dati.toscana.it/dataset/dem10mt

    - Provincia Autonoma di Trento: LIDAR rilievo 2006/2007/2008 (= DTM 2 m)
http://dati.trentino.it/dataset/lidar-rilievo-2006-2007-2008-link-al-servizio-di-download

    - Autonome Provinz Bozen-Südtirol: DTM 5 m
http://www.provinz.bz.it/natur-raum/themen/landeskartografie-download.asp

    - Regione Autonoma Valle d'Aosta: DTM 2005/2008 aggregato (voli laser scanner 2005 e 2008), intero territorio regionale (= DTM 2 m)
http://geoportale.partout.it/prodotti_cartografici/repertorio_cartografico/dtm/default_i.aspx

    - Regione Veneto: DTM regionale con celle di 5 metri di lato (= DTM 5 m)
http://dati.veneto.it/dataset/dtm-regionale-con-celle-di-5-metri-di-lato

    - Malta Planning Authority: DTM 10 m
https://www.pa.org.mt/

    - NASA: SRTM Version 3.0 Global 1" DTMs
http://dwtkns.com/srtm30m/


- Switzerland: DEM 20m (compiled and resampled von Sonny;
 Website: http://data.opendataportal.at/dataset/dtm-switzerland)

    - Kanton Aargau: DTM 5m
https://www.ag.ch/de/dfr/geoportal/geoportal.jsp

    - Kanton Basel-Landschaft: DTM 5m
https://www.geo.bl.ch/index.php?id=75

    - Kanton Basel-Stadt: DTM 2m
http://www.geo.bs.ch/

    - Kanton Bern: DTM 0.5m
https://www.geo.apps.be.ch/de/

    - République et canton de Genève (Kanton Genf): DTM 0.5m
https://www.etat.ge.ch/geoportail/pro/

    - Kanton Glarus: DTM 0.5m
http://www.gl.ch/default.cfm?treeid=1736&domainID=1&languageID=1

    - Kanton Graubünden: DTM 2m
https://geogr.mapplus.ch/shop/

    - Kanton Schaffhausen: DTM 0.5m
https://www.sh.ch/Amt-fuer-Geoinformation.238.0.html

    - Kanton Schwyz: DTM 2m
https://www.sz.ch/behoerden/vermessung-geoinformation/geoportal/geoportal.html/72-416-414-1762-1744

    - Kanton Solothurn: DTM 0.5m
https://www.so.ch/verwaltung/bau-und-justizdepartement/amt-fuer-geoinformation/geoportal/

    - Kanton Uri: DTM 2m
https://geo.ur.ch/

    - Kanton Zürich: DTM 0.5m
https://are.zh.ch/internet/baudirektion/are/de/geoinformation/geodaten_uebersicht/geodatenshop.html


Peaks, Valleys, Mountain ranges, Gondolas, Motorways, Roads, Trails, Lakes, Cities, Towns:

OpenStreetMap, ODbL; Website: www.opendatacommons.org, www.openstreetmap.org/copyright 


